$(document).ready(function () {
    $('.post_card').on('click', function () {
        let rootUrl = $('body').data('root-url');
        let postId = $(this).attr('id');
        let route = rootUrl + 'post/view/' + postId;
        $( location ).attr("href", route);
    });
});