<?php

namespace App\DataFixtures;

use App\Entity\User;
use App\Model\User\UserHandler;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class UserFixtures extends Fixture
{
    private $userHandler;

    public function __construct(UserHandler $userHandler)
    {
        $this->userHandler = $userHandler;
    }

    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $usernames = ['root', 'boot', 'doot'];
        $password = $this->userHandler->passwordHashing('root');

        foreach ($usernames as $key => $username){
            $user = new User();
            $user
                ->setUsername($username)
                ->setPassword($password);
            $manager->persist($user);

            $this->addReference($username, $user);
        }
        $manager->flush();
    }

}