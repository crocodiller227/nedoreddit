<?php

namespace App\DataFixtures;

use App\Entity\Post;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class PostFixtures extends Fixture implements DependentFixtureInterface
{

    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $users = [];
        $usernames = ['root', 'boot', 'doot'];
        foreach ($usernames as $username) {
            $users[] = $this->getReference($username);
        }

        $posts = ["t3_7e2x3e", "t3_8loxz1", "t3_80607r", "t3_8cgj3g", "t3_8g1n0z",
            "t3_8qa3p9", "t3_8ouvwv", "t3_8qy0td", "t3_8wgone", "t3_8widua", "t3_8sh3vu",
            "t3_8qaaoq", "t3_7x1mc1", "t3_8gncs3", "t3_8qep03", "t3_8talwl", "t3_8uwp0l", "t3_7qbx7x",
            "t3_8sqpki", "t3_84nz9p", "t3_8l8nkv", "t3_8hn9ee", "t3_8g1giq"];

        foreach ($posts as $postName) {
            $post = new Post();

            $post->setName($postName)
                ->addUser($users[0]);

            if (rand(1, 0)) {
                $post->addUser($users[1]);
            }
            if (rand(1, 0)) {
                $post->addUser($users[2]);
            }
            $manager->persist($post);

        }
        $manager->flush();
    }

    public function getDependencies()
    {
        return [
            UserFixtures::class
        ];
    }
}