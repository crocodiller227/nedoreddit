<?php

namespace App\Repository;

use App\Entity\Post;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\Query;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Post|null find($id, $lockMode = null, $lockVersion = null)
 * @method Post|null findOneBy(array $criteria, array $orderBy = null)
 * @method Post[]    findAll()
 * @method Post[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PostRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Post::class);
    }

    /**
     * @return Post[] Returns an array of Post objects
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findByNameField($name)
    {
        return $this->createQueryBuilder('p')
            ->select('p')
            ->where('p.name = :name')
            ->setParameter('name', $name)
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult();
    }

    public function findAllWithOrder($page)
    {
        $to = $page * 5;
        $from = $to - 5;
        return $this->createQueryBuilder('p')
            ->select('COUNT(u) AS HIDDEN posts', 'p')
            ->leftJoin('p.users', 'u')
            ->orderBy('posts', 'DESC')
            ->groupBy('p')
            ->setFirstResult($from)
            ->setMaxResults(5)
            ->getQuery()
            ->getResult();
    }

    public function getFavoriteQuantity($name)
    {
        return $this->createQueryBuilder('p')
           ->select('COUNT(u) AS HIDDEN posts', 'p')
            ->where('p.name = :name')
            ->setParameter('name', $name)
            ->leftJoin('p.users', 'u')
            ->groupBy('p')
            ->getQuery()
            ->getResult();
    }

    public function findUserPostsWithPagination($userId, $page)
    {
        $to = $page * 5;
        $from = $to - 5;
        return $this->createQueryBuilder('p')
            ->select('u AS HIDDEN posts', 'p')
            ->leftJoin('p.users', 'u')
            ->where('u.id = :userId')
            ->setParameter('userId', $userId)
            ->setFirstResult($from)
            ->setMaxResults(5)
            ->getQuery()
            ->getResult();
    }

    public function getCountPosts()
    {
        try {
            return $this->createQueryBuilder('p')
                ->select('COUNT(p)')
                ->getQuery()
                ->getSingleScalarResult();
        } catch (NonUniqueResultException $e) {
            return 0;
        }
    }


}
