<?php


namespace App\Controller;


use App\Entity\Post;
use App\Model\Api\ApiContext;
use App\Model\Api\ApiException;
use App\Model\Error\ErrorHandler;
use App\Model\Posts\PostHandler;
use App\Repository\PostRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\NonUniqueResultException;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class PostController
 * @package App\Controller
 * @Route("/post")
 */
class PostController extends Controller
{
    /**
     * @Route("/favorite/add/{post_name}", name="to_favorite")
     * @param string $post_name
     * @param EntityManagerInterface $entityManager
     * @param PostRepository $postRepository
     * @param ErrorHandler $errorHandler
     * @return RedirectResponse
     * @throws NonUniqueResultException
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function addPostInFavoritesAction(
        string $post_name,
        EntityManagerInterface $entityManager,
        PostRepository $postRepository,
        ErrorHandler $errorHandler
    )
    {
        if (!$postRepository->findByNameField($post_name)) {
            $post = new Post();
            $post->addUser($this->getUser());
            $post->setName($post_name);
            $entityManager->persist($post);
            $entityManager->flush();
        } else {
            $errorHandler->setErrorIntoSession('Fasting is already in the chosen');
            return $this->redirectToRoute('index_page');
        }
        return $this->redirectToRoute('index_page');
    }

    /**
     * @Route("/favorite/show/{page}", name="favorite_posts")
     * @param ApiContext $apiContext
     * @param PostHandler $postHandler
     * @param int $page
     * @param PostRepository $postRepository
     * @return Response
     * @throws ApiException
     */
    public function favoritesPostsAction(
        ApiContext $apiContext,
        PostHandler $postHandler,
        int $page,
        PostRepository $postRepository
    )
    {
        $posts = $postRepository->findUserPostsWithPagination($this->getUser()->getId(), $page);
        $postNames = [];
        if ($posts) {
            foreach ($posts as $post) {
                $postNames[] = $post->getName();
            }
        }
        $posts = $apiContext->getFavorites($postNames)['data']['children'];
        $posts = $postHandler->setPostFavoriteQty($posts);
        $pages = ceil(count($this->getUser()->getPosts()) / 5);
        return $this->render('post/favorites.html.twig', [
            'posts' => $posts,
            'pages' => $pages,
            'page' => $page
        ]);
    }

    /**
     * @Route("/view/{post_name}")
     * @param string $post_name
     * @param ApiContext $apiContext
     * @param PostHandler $postHandler
     * @return Response
     * @throws ApiException
     */
    public function showPostPage(
        string $post_name,
        ApiContext $apiContext,
        PostHandler $postHandler
    )
    {
        $post = $apiContext->getFavorites([$post_name])['data']['children'][0];
        $post = $post = $postHandler->setPostFavoriteQty([$post]);
        return $this->render('post/show_post.html.twig', [
            'post' => $post[0] ?? null
        ]);
    }

    /**
     * @Route("/favorite/community/{page}", name="community_favorite")
     * @param PostRepository $postRepository
     * @param ApiContext $apiContext
     * @param ErrorHandler $errorHandler
     * @return Response
     * @throws ApiException
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function favoriteCommunityAction(
        PostRepository $postRepository,
        ApiContext $apiContext,
        ErrorHandler $errorHandler,
        PostHandler $postHandler,
        int $page
    )
    {
        $error = null;
        $postsFromBase = $postRepository->findAllWithOrder($page);
        $postNames = [];
        foreach ($postsFromBase as $post) {
            $postNames[] = $post->getName();
        }
        $posts = $apiContext->getFavorites($postNames)['data']['children'];
        $posts = $postHandler->setPostFavoriteQty($posts);

        $error = $errorHandler->getErrorMessageOfSession($error);

        $pages = ceil($postRepository->getCountPosts() / 5);

        return $this->render('/post/comminity_favorite.html.twig', [
            'posts' => $posts,
            'error' => $error,
            'pages' => $pages,
            'page' => $page
        ]);
    }
}