<?php


namespace App\Controller;


use App\Form\FilterFormType;
use App\Model\Api\ApiContext;
use App\Model\Api\ApiException;
use App\Model\Error\ErrorHandler;
use App\Model\Posts\PostHandler;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class IndexController extends Controller
{
    /**
     * @Route("/", name="index_page")
     * @param ErrorHandler $errorHandler
     * @param ApiContext $apiContext
     * @param Request $request
     * @param PostHandler $postHandler
     * @return Response
     * @throws ApiException
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function indexAction(
        ErrorHandler $errorHandler,
        ApiContext $apiContext,
        Request $request,
        PostHandler $postHandler
    )
    {
        $error = null;
        $filterForm = $this->createForm(FilterFormType::class);
        $filterForm->handleRequest($request);
        if ($filterForm->isSubmitted() && $filterForm->isValid()) {
            $data = $filterForm->getData();
            $posts = $apiContext->getPosts($data)['data']['children'];
            $posts = $postHandler->setPostFavoriteQty($posts);

        }
        $error = $errorHandler->getErrorMessageOfSession($error);
        return $this->render('index.html.twig', [
            'error' => $error,
            'filter_form' => $filterForm->createView(),
            'posts' => $posts ?? null
        ]);
    }


}