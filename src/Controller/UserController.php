<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\RegisterOrLoginUserType;
use App\Model\Error\ErrorHandler;
use App\Model\User\UserHandler;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class UserController
 * @Route("/user")
 */
class UserController extends Controller
{
    /**
     * @Route("/register", name="register_user")
     * @param Request $request
     * @param EntityManagerInterface $entityManager
     * @param UserHandler $userHandler
     * @param ErrorHandler $errorHandler
     * @return Response
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function registerUserAction(
        Request $request,
        EntityManagerInterface $entityManager,
        UserHandler $userHandler,
        ErrorHandler $errorHandler
    )
    {
        $error = null;
        $user = new User();
        $userRegisterForm = $this->createForm(RegisterOrLoginUserType::class, $user);

        $userRegisterForm->handleRequest($request);
        if ($userRegisterForm->isSubmitted() && $userRegisterForm->isValid()) {
            try {
                $user->setPassword($userHandler->passwordHashing($user->getPassword()));
                $entityManager->persist($user);
                $entityManager->flush();
            } catch (Exception $e) {
                $errorHandler->setErrorIntoSession($e->getMessage());
                return $this->redirectToRoute('register_user');
            }
            return $this->redirectToRoute('login_user');
        }
        $error = $errorHandler->getErrorMessageOfSession($error);
        return $this->render('user/register.html.twig', [
            'form' => $userRegisterForm->createView(),
            'error' => $error
        ]);
    }

    /**
     * @Route("/login", name="login_user")
     * @param Request $request
     * @param ErrorHandler $errorHandler
     * @return Response
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function loginAction(
        Request $request,
        ErrorHandler $errorHandler,
        UserRepository $userRepository,
        UserHandler $userHandler
    )
    {
        $error = null;
        $user = null;
        $userLoginForm = $this->createForm(RegisterOrLoginUserType::class);
        $userLoginForm->handleRequest($request);

        if ($userLoginForm->isSubmitted() && $userLoginForm->isValid()) {
            $data = $userLoginForm->getData();
            try {
                $user = $userRepository->findUserByUsernameAndPassword(
                    $data['username'],
                    $userHandler->passwordHashing($data['password'])
                );
            } catch (Exception $e) {
                $error = $e->getMessage();
            }

        }
        if ($user) {
            $userHandler->insertUserInSession($user);
            return $this->redirectToRoute('index_page');
        }

        $error = $errorHandler->getErrorMessageOfSession($error);

        return $this->render('user/login.html.twig', [
            'error' => $error,
            'form' => $userLoginForm->createView()
        ]);
    }
}