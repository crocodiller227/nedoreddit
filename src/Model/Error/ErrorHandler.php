<?php

namespace App\Model\Error;

use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class ErrorHandler
{
    private $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * @param $error
     * @return mixed
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function getErrorMessageOfSession($error)
    {
        if (!$error) {
            $session = $this->container->get('session');
            if ($session->has('err_show_user')) {
                $error = $session->get('err_show_user');
                $session->remove('err_show_user');
            }
        }
        return $error;
    }

    /**
     * @param $error
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function setErrorIntoSession($error)
    {
        $this->container->get('session')->set('err_show_user', $error);
    }
}