<?php

namespace App\Model\Posts;

use App\Repository\PostRepository;

class PostHandler
{
    private $postRepository;

    /**
     * PostHandler constructor.
     * @param PostRepository $postRepository
     */
    public function __construct(PostRepository $postRepository)
    {
        $this->postRepository = $postRepository;
    }

    public function setPostFavoriteQty($posts)
    {
        foreach ($posts as $key => $post) {
            $favoriteQty = count($this->postRepository->getFavoriteQuantity($post['data']['name']));
            $posts[$key]['data']['__in_favorites'] = ($favoriteQty === null) ? 0 : $favoriteQty;
        }

        return $posts;
    }
}