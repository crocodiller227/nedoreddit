<?php


namespace App\Model\Api;


class ApiContext extends AbstractApiContext
{
    const ENDPOINT_GET_REDDIT_POSTS = '/r/picture/search.json?q=#search#&sort=#sort#&limit=#limit#&type=link';
    const ENDPOINT_GET_FAVORITES = '/api/info.json?id=';

    /**
     * @return mixed
     * @throws ApiException
     */
    public function getPosts($data)
    {
        $endpoint = $this->generateApiUrl(self::ENDPOINT_GET_REDDIT_POSTS, $data);
        return $this->makeQuery($endpoint);
    }
    /**
     * @return mixed
     * @throws ApiException
     */
    public function getFavorites($data)
    {
        $endpoint = $this->generateUrlWithRepeatParam(self::ENDPOINT_GET_FAVORITES, $data);
        return $this->makeQuery($endpoint);
    }


}