<?php

namespace App\Model\Api;

use Curl\Curl;
use Symfony\Component\DependencyInjection\ContainerInterface;

class AbstractApiContext
{

    /** @var Curl */
    protected $curl;

    /** @var ContainerInterface */
    protected $container;

    /** @var string */
    protected $apiUrl;

    /**
     * ApiContext constructor.
     * @param ContainerInterface $container
     * @throws \ErrorException
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;

        $this->apiUrl = $this->getParameter('reddit')['api']['url'];
        $this->curl = new Curl();
        $this->curl->setJsonDecoder(function () {
            $args = func_get_args();

            $response = json_decode($args[0], true);
            if ($response === null) {
                $response = $args[0];
            }
            return $response;
        });
    }

    protected function generateApiUrl(string $rawEndpoint, array $params)
    {
        $result = $rawEndpoint;
        foreach ($params as $paramName => $paramValue) {
            $result = str_replace("#{$paramName}#", urlencode($paramValue), $result);
        }

        return $result;
    }

    protected function generateUrlWithRepeatParam(string $rawEndpoint, array $params)
    {
        $endpoint = $rawEndpoint;
        foreach ($params as $param){
            $endpoint .= $param . ',';
        }
        return substr($endpoint,0,-1);
    }

    /**
     * @param string $endpoint
     * @param array $data
     * @return mixed
     * @throws ApiException
     */
    protected function makeQuery(string $endpoint, array $data = [])
    {
        $url = $this->apiUrl . $endpoint;
        $this->curl->setHeader(
            'user-agent',
            'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.186 Safari/537.36 OPR/51.0.2830.55'
        );
        $this->curl->get($url, $data);
        if ($this->curl->error) {
            throw new ApiException(
                $this->curl->errorMessage,
                $this->curl->errorCode,
                null,
                $this->curl->response
            );
        } else {
            return $this->curl->response;
        }
    }

    protected function getParameter(string $name)
    {
        return $this->container->getParameter($name);
    }
}
