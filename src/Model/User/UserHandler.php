<?php


namespace App\Model\User;


use App\Entity\User;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;

class UserHandler
{

    /**
     * @var ContainerInterface
     */
    private $container;


    public function __construct(
        ContainerInterface $container
    )
    {
        $this->container = $container;
    }

    /**
     * @param string $plainPassword
     * @return bool|string
     */
    public function passwordHashing(string $plainPassword)
    {
        return password_hash($plainPassword,
            PASSWORD_BCRYPT,
            ['cost' => 12, 'salt' => md5('reddit-shmeddit')]);
    }

    /**
     * @param User $user
     */
    public function insertUserInSession(User $user)
    {
        $token = new UsernamePasswordToken($user, null, 'main', $user->getRoles());
        $this
            ->container
            ->get('security.token_storage')
            ->setToken($token);
        $this
            ->container
            ->get('session')
            ->set('_security_main', serialize($token));
    }
}