<?php


namespace App\Form;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;

class FilterFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('search')
            ->add('sort', ChoiceType::class, [
                'choices'=>[
                    'Relevance' => 'relevance',
                    'Hot' => 'hot',
                    'In top' => 'top',
                    'New' => 'new',
                    'Comments' =>'comments',
                ]
            ])
            ->add('limit', ChoiceType::class, [
                'choices' => [
                    '5pc' => 5,
                    '10pc' => 10,
                    '15pc' => 15,
                    '20pc' => 20,
                    '30pc' => 30,
                    '40pc' => 40,
                ]
            ])
            ->add('filter', SubmitType::class);
    }
}